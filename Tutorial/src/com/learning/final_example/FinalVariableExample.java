/**
 * 
 */
package com.learning.final_example;

/**
 * @author Piyush
 *
 */
public class FinalVariableExample {

	public static final String FINAL_1 = "Constant variable name should be in upper case";
	/**
	 * Final variable which is not initialized at the time of declaration is called 
	 * "blank final" variable. It can be only initialized in constructor if not initialized tha the time if declaration
	 * We will be using this if we want to create immutable field(i.e. like we want to store some information in instance
	 * which should not be modified)
	 */
	public final String FINAL_2;

	FinalVariableExample() {
		FINAL_2 = "Instance variable with final should be initialized at the time of declaration or "
				+ "it should be initialized in constructor";
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(FINAL_1);
		System.out.println(new FinalVariableExample().FINAL_2);

	}

}
