/**
 * 
 */
package com.learning.final_example;

/**
 * @author Piyush
 *
 */
public class FinalMethodExample {
	
	
	public void method1() {
		System.out.println("from method 1");
	}
	
	public final void method2() {
		System.out.println("From method 2");
	}
	
	public static void main(String[] args) {
		new FinalMethodExampleChild().method1();
		
		new FinalMethodExample().method1();
		/**
		 * Below line will print same content as both will invole super class method 2
		 */
		new FinalMethodExampleChild().method2();
		new FinalMethodExample().method2();
	}


}

class FinalMethodExampleChild extends FinalMethodExample {
	
	@Override
	public void method1() {
		System.out.println("From method 1 of child class");
	}
	
	/**
	 * Below method will throw error saying
	 * "Cannot override the final method from FinalMethodExample"
	 */
	/*
	 * @Override public void method2() {
	 * System.out.println("From method 1 of child class"); }
	 */
	
	
	
}
