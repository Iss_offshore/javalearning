package com.learning.final_example;

/**
 * @author Piyush
 * FinalClassAndMethodExample is declared with final keyword so this class can not have sub class
 * i.e. <Some class > extends FinalClassAndMethodExample will throwError
 * We will be defining this kind of class for some confidential functionality like if we want to have a class
 * which will encrypt and decrypt some confidential information then we will be using final to make class secure from 
 * inheritance.
 * If we will not make it final any one can extend this class and can modify the encryption and decryption 
 * functionality
 */
public final class FinalClassExample {
	
	public void test() {
		System.out.println("from test method");
	}

}

/**
 * Below line will throw error sayiong
 * "The type testFinalClass cannot subclass the final class FinalClassAndMethodExample"
 */

// class testFinalClass extends FinalClassAndMethodExample {}
