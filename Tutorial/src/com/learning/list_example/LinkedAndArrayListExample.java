/**
 * 
 */
package com.learning.list_example;

/**
 * @author Piyush
 *
 */
public class LinkedAndArrayListExample {
	
	/**
	 * Arraylist should be used when we have more read operation than write operation because write operation will be 
	 * very heavy and it will take lot of cpu and memory whereas read process is very fast on ArrayList 
	 */
	
	/**
	 * LinkedList shoudl be used when we have more write operation than read operation because read operation of LInkedList
	 * is heavy and write operation of LinkedList is very light
	 */

}
