package com.learning.abstract_example;

/**
 * @author Piyush
 * https://www.javatpoint.com/abstract-class-in-java
 * Abstraction is the process of hiding actual functionality from user and it will be added runtime.
 * Abstract class can have complete or abstract method
 */
public abstract class AbstractClassExample {
	
	public void concreteMethod() {
		System.out.println("Message from abstract class concrete method");
	}
	
	/**
	 *  Child class has to define the functionality of this method if child class dont want to define its functionality
	 *  then child class should and must have abstract class
	 */
	public abstract void abstractMethod();

}
