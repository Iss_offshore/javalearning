/**
 * 
 */
package com.learning.abstract_example;

/**
 * @author Piyush
 *
 */
public class AbstractClassResult {
	
	public static void main(String[] args) {
		
		AbstractClassExample  example1 = new AbstractClassExampleImpl1();
		AbstractClassExample  example2 = new AbstractClassExampleImpl3();
		
		AbstractClassExampleImpl1  exampleImpl1 = new AbstractClassExampleImpl1();
		AbstractClassExampleImpl3  exampleImpl3 = new AbstractClassExampleImpl3();
		
		example1.concreteMethod();
		example1.abstractMethod();
		
		example2.concreteMethod();
		example2.abstractMethod();
		
		exampleImpl1.concreteMethod();
		exampleImpl1.abstractMethod();
		
		exampleImpl3.concreteMethod();
		exampleImpl3.abstractMethod();
		
	}

}
