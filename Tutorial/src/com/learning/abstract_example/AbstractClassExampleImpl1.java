/**
 * 
 */
package com.learning.abstract_example;

/**
 * @author Piyush
 *
 */
public class AbstractClassExampleImpl1 extends AbstractClassExample {

	@Override
	public void abstractMethod() {
		System.out.println("Message from abstract method of AbstractClassExampleImpl1");

	}

}
