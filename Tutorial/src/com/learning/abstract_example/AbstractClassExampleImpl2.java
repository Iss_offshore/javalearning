/**
 * 
 */
package com.learning.abstract_example;

/**
 *If we want to have concrete class without defining functionalty of super class it will throw error saying
 * "The type AbstractClassExampleImpl2 must implement the inherited abstract method AbstractClassExample.abstractMethod()"
 *
 */
//public class AbstractClassExampleImpl2 extends AbstractClassExample {


//}
