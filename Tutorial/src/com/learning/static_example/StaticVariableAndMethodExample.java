/**
 * 
 */
package com.learning.static_example;

/**
 * @author Piyush
 *
 */
public class StaticVariableAndMethodExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		/**
		 * Below line will print 75.36 i.e. 2 * 3.14 * 12.0
		 */
		System.out.println(CircleStaticExample.calculateCircumference(12.0));
		/**
		 * Below line will print value of PI i.e. 3.14
		 */
		CircleStaticExample.staticExample();
		/**
		 * Below line will print 3.14 and 0 i.e. value of PI and default value of instance variable
		 */
		new CircleStaticExample().instanceMethiodExmaple();

	}

}
