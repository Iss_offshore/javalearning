package com.learning.static_example;

/**
 * @author Piyush
 * Class is responsible to do operation on circle
 *
 */
public class CircleStaticExample {
	
	/**
	 * Description of below line
	 * private :- We are going to use this variable inside this class so we declared it as private
	 * static :- I want to share this variable among all static and non static method so declared it as static
	 * final :- I want to declare this variable as constant as value of PI is not going to change
	 */
	private static final Double PI = 3.14;
	private Integer instanceVariable = 0;
	
	/**
	 * Example of static variables and methods
	 * @param radius
	 * @return
	 */
	public static Double calculateCircumference(Double radius) {
		return 2 * PI * radius;
	}
	
	public static void staticExample() {
		/**
		 * Below line will throw 
		 * "Cannot make a static reference to the non-static field instanceVariable" error because we are trying to
		 * access instance variable from static method
		 */
		// System.out.println(instanceVariable)
		/***
		 * Below line will work fine because static variable can be access in static method
		 */
		System.out.println(PI);
	}
	
	public void instanceMethiodExmaple() {
		/**
		 * Below line will work fine because instance method can access static variable as well as instance variable.
		 */
		System.out.println(PI);
		System.out.println(instanceVariable);
	}
	
	

}
