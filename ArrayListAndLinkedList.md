** Ref document :- **

https://www.javatpoint.com/difference-between-arraylist-and-linkedlist

https://www.geeksforgeeks.org/arraylist-vs-linkedlist-java/


** Explanation :- **
# ArrayList :- #
	https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html
# LinkedList :- #
	https://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html
	
### Program Reference
	Please refer **com.learning.list_example** package
	
	
	
