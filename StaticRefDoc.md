** Ref document :- **

https://csawesome.runestone.academy/runestone/books/published/csawesome/Unit5-Writing-Classes/topic-5-7-static-vars-methods.html

https://www.tutorialspoint.com/demonstrate-static-variables-methods-and-blocks-in-java


** Explanation :- **
# Static Variable :- #
	1. Why do we need static variables or methods?
		If We want to share data among all the instance of the associated class and we dont want to allocate memory for variables when we create instance of class in that case we will prefer static variable. **JVM will keep single copy of static variable.**
	2. Life cycle of static variable :-
		JVM will load static variables in memory at the time of class loading and it will stay in memory untill class unloads from memory.
	3. Who can access static variables 
		**Static variables will be available for both static as well as instance methods.**
	
# Static Methods :- #
	1. Why we need?
		If we want to execute a part of fucntionality without creating instance of class in other word we can say if we want to perform some utility functionality like formatting a number and etc there we will be declaring our method as static.
	2. What will be accessible for static method :
		All the static variable will be accessible for static method but **instance variable and methods can not be accessd by static methods whereas instance variable and method can access static methods**.
		
# How to declare static variables :- #
	**access_type** **static keyword** **data type** **variableName** = **default value**
	### access_type can be public,protected,default and private
	### default value is optional
# Program Reference :- #
	Please refer **StaticVariableAndMethodExample.java and StaticVariableAndMethod,java** for examples
	
