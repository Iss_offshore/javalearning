** Ref document :- **

https://www.javatpoint.com/final-keyword

## Explanation :- 
	Final is a keyword in java and it can be used for below objects
	1. Class
	2. Method 
	3. Variable

### Class 
	When final is used with class then class can not have child class i.e. other class can not extends class defined with final keyword.
### Method 
	When final is used with a method then no one can overirde it even though class in not final.
### Variable 
	When final is used with variable JVM will treat it as constant. Generally we use final keyword with variables to define constant.
### Explanation 
a. When to use final with class :- Suppose I wanted to keep my functionality concrete i.e. once I will create a functionality no one can modify it and everyone has to use the same.
	

# Program Reference :- #
	Please refer **com.learning.final_example** package for examples
	
